from flask import request
from functools import wraps
import db

def auth_required(fn):
    @wraps(fn)
    def inner(*args, **kwargs):
        if db.AccessToken.is_valid(request.headers.get("Authorization")):
            return fn(*args, **kwargs)
        else:
            return None

    return inner
