# web-server

App Server of the Freelance Coop App. 

This is a Flask Web App using Postgresql. It MUST be deployable as a docker server that can be scaled to run on multiple instances.


## Installation (with Docker)
```bash
$ git clone https://gitlab.com/devcoop1/app-server
$ cd app-server
$ docker-compose up --build -d
```

## Usage


## Support
Helping the development of this project is very much welcome.


## Contributing

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
This project is licensed under the AGPL v3 or later.

## Project status

