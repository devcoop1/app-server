import peewee as pw

class EnumField(pw.Field):
    field_type = "text"

    def __init__(self, enum, *args, **kwargs):
        super(EnumField, self).__init__(*args, **kwargs)

        self.__enum = enum

    def db_value(self, value):
        return value.name

    def python_value(self, value):
        return self.__enum[value]
