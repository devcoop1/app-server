from ariadne import load_schema_from_path, make_executable_schema
from .query import query
from .user import user
from .group import group

schema = make_executable_schema(load_schema_from_path("../graphql"), [query, user, group])

