class InvalidUserError(Exception):
    def __init__(self, user):
        self.__user = user

class InvalidRoleError(Exception):
    def __init__(self, role):
        self.__role = role

class InvalidEmailOrName(Exception):
    def __init__(self, email_or_name):
        self.__email_or_name = email_or_name
