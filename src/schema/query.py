from ariadne import QueryType, ObjectType
from flask import request
from .auth import auth_required
import db

query = QueryType()

@query.field("user")
@auth_required
def resolve_user(_, info):
    user = db.AccessToken.get_user(request.headers.get("Authorization"))

    return user

@query.field("payments")
@auth_required
def resolve_outstanding_payments(_, info, completed, receiver, payer):
    user = db.AccessToken.get_user(request.headers.get("Authorization"))

    payments = db.Payment.select().where(db.Payment.receiver == user | db.Payment.payer == user)

    if completed is not None:
        payments = payments.where(db.Payment.completed is completed)

    if receiver is not None:
        payments = payments.where(db.Payment.receiver == receiver)

    if payer is not None:
        payments = payments.where(db.Payment.payer == payer)

    return payments


@query.field("tasks")
@auth_required
def resolve_tasks(_, info, evaluator, worker, customer, stage):
    user = db.AccessToken.get_user(request.headers.get("Authorization"))

    tasks = db.Task.select().where(db.Task.customer == user | db.Task.group == user.group)

    if evaluator is not None:
        tasks = tasks.where(evaluator << db.Task.evaluators)

    if worker is not None:
        tasks = tasks.where(worker << db.Task.workers)

    if customer is not None:
        tasks = tasks.where(db.Task.customer == customer)

    if stage is not None:
        tasks = tasks.where(db.Task.stage == stage)

    return tasks
