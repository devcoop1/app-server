"""
Provides the flask app object.
"""
from flask import Flask, request, jsonify
from ariadne import graphql_sync
from ariadne.constants import PLAYGROUND_HTML
from schema import schema
import os
import db

app = Flask(__name__)
app.debug = "DEBUG" in os.environ


@app.route("/api/graphql", methods=["GET"])
def graphql_playground():
    return PLAYGROUND_HTML, 200


@app.route("/api/graphql", methods=["POST"])
def graphql():

    data = request.get_json()
    success, result = graphql_sync(
        schema,
        data,
        context_value=request,
        debug=app.debug,
    )

    status_code = 200 if success else 400
    return jsonify(result), status_code
