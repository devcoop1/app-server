def filter_tasks(tasks, primary_evaluator, primary_worker, evaluator, worker, customer, stage):
    if evaluator is not None:
        evaluator = db.User.get_or_none(db.User.id == evaluator)

        tasks = tasks.where(evaluator << db.Task.evaluators)

    if worker is not None:
        worker = db.User.get_or_none(db.User.id == worker)
        tasks = tasks.where(worker << db.Task.workers)

    if customer is not None:
        tasks = tasks.where(db.Task.customer.id == customer)

    if stage is not None:
        tasks = tasks.where(db.Task.stage == stage)
