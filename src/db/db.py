from .enum_field import EnumField
from .exceptions import InvalidUserError
from enum import Enum
import peewee as pw
import time
import os

DATABASE = os.environ["DB_NAME"]
password = os.environ["DB_PASSWORD"]
user = os.environ["DB_USER"]
host = os.environ["DB_HOST"]

database = pw.PostgresqlDatabase(DATABASE,password=PASSWORD, user=USER, host=HOST)

class BaseModel(pw.Model):
    class Meta:
        database = database

# User
class Membership(Enum):
    GUEST = 0
    TRIAL = 1
    PERMANENT = 2
    MODERATOR = 3
    TREASURER = 4s

class User(BaseModel):
    id = pw.UUIDField(primary_key=True)

    password_hash = pw.BlobField()
    salt = pw.BlobField()

    group = pw.DeferredForeignKey("Group", null = True, backref="members")
    membership = EnumField(Membership)

    name = pw.CharField()
    email = pw.CharField(unique=True)
    publicKey = pw.TextField()

    def apply_group(self, group):
        self.group = group
        self.membership = Membership.GUEST

    def accept_trial_member(self, user):
        if user.group.id != self.group.id or user.membership is not Membership.GUEST:
            raise InvalidUserError(user)


        if self.membership is not Membership.MODERATOR:
            raise InvalidRoleError(self.membership)

        user.membership = Membership.TRIAL
        user.save()

class AccessToken(BaseModel):
    token = pw.CharField(primary_key=True)
    user = pw.ForeignKeyField(User, backref="access_tokens")
    expires = pw.IntegerField()


    def generate(email_or_name, password):
        users = User.select().where(User.email == email_or_name | User.name == email_or_name)

        if not users.exists():
            raise InvalidEmailOrNameError()

        user = users.get()

        if user.password_hash != SHA3_256.new(password.encode("utf-8") + salt).digest():
            raise InvalidPasswordError()

        token = base64.b64encode(get_random_bytes(32))
        expires = time.time() + 43200 # 60*60*12

        return AccessToken.create(
            token=token,
            user=user,
            expires=expires
        )

    def is_valid(token):
        token = AccessToken.get_or_none(AccessToken.token == token)

        if token is None:
            return False

        return time.time() < token.expires

    def get_user(token):
        token = AccessToken.get_or_none(AccessToken.token == token)

        if token is None:
            return None

        return token.user




# Group
class Group(BaseModel):
    id = pw.UUIDField(primary_key=True)

    name = pw.CharField()
    website = pw.TextField()


class Payment(BaseModel):
    id = pw.UUIDField(primary_key=True)
    receiver = pw.ForeignKeyField(User, backref="receiving")
    payer = pw.ForeignKeyField(User, backref="payments")

    reason = pw.TextField()
    completed = pw.BooleanField(default=False)

class Transaction(BaseModel):
    payment = pw.ForeignKeyField(Payment, backref = "transactions")
    id = pw.TextField()

    src = pw.TextField()
    dest = pw.TextField()
    amount = pw.IntegerField()

    completed = pw.BooleanField(default=False)

# Task
class Stage(Enum):
    PROPOSED = 1
    APPROVED = 2
    ASSIGNED = 3
    COMPLETED = 4

class Task(BaseModel):
    id = pw.UUIDField(primary_key=True)
    group = pw.ForeignKeyField(Group, backref="tasks")

    customer = pw.ForeignKeyField(User, backref="tasks_as_customer")

    primary_evaluator = pw.ForeignKeyField(User, backref="tasks_as_primary_evaluator") # consent required to add evaluators
    primary_worker = pw.ForeignKeyField(User, backref="tasks_as_primary_worker") # consent required to add workers

    evaluators = pw.ManyToManyField(User, backref="tasks_as_evaluator") # majority of evaluators consent required for approval
    workers = pw.ManyToManyField(User, backref="tasks_as_worker") # majority of workers consent required for completion

    stage = EnumField(Stage)

    customer_proposal = pw.DeferredForeignKey("TaskProposal", backref="task")
    evaluators_proposal = pw.DeferredForeignKey("TaskProposal", backref="task")

    accepted_proposal = pw.DeferredForeignKey("TaskProposal", backref="task") # accepted by both the majority of evaluators and the customer.

class TaskProposal(BaseModel):
    proposers = pw.ManyToManyField(User, backref="proposals")

    description = pw.TextField()

    payment = pw.IntegerField()
    escrow_fee = pw.IntegerField()

    accepted_by = pw.ManyToManyField(User, backref="accepted_proposals")





