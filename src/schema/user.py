from ariadne import ObjectType
import db

user = ObjectType("User")


@user.field("tasks")
def resolve_tasks(user, info, primary_evaluator, primary_worker, evaluator, worker, customer, stage):

    tasks = db.Task.select().where(user << db.Task.evaluators | user << db.Task.workers)

    if primary_evaluator is not None:
        tasks = tasks.where(user == db.Task.primary_evaluator)

    if primary_worker is not None:
        tasks = tasks.where(user == db.Task.primary_worker)

    if evaluator is not None:
        tasks = tasks.where(user << db.Task.evaluators)

    if worker is not None:
        tasks = tasks.where(user << db.Task.workers)

    if customer is not None:
        tasks = tasks.where(user == db.Task.customer)

    if stage is not None:
        tasks = tasks.where(db.Task.stage == stage)

    return tasks

